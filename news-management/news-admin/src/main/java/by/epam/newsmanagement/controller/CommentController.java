package by.epam.newsmanagement.controller;

import by.epam.newsmanagement.domain.Comment;
import by.epam.newsmanagement.service.ICommentService;
import by.epam.newsmanagement.service.exception.ServiceException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Date;

@Controller
@RequestMapping(value = "/comment")
public class CommentController {
    @Autowired
    private ICommentService commentService;
    private static final Logger LOG = LogManager.getLogger(CommentController.class);
    private static final String NEWS_ID_KEY = "news_id";
    private static final String COMMENT_ID_KEY = "comment_id";
    @ExceptionHandler(Exception.class)
    public String handleAllException(Exception e) {
        LOG.error(e);
        return "error";
    }

    @RequestMapping(value = "add/{news_id}", method = RequestMethod.POST)
    public String addComment(@Valid Comment comment, BindingResult bindingResult,
                             @PathVariable(NEWS_ID_KEY) Long news_id) throws ServiceException {
        if (bindingResult.hasErrors()) {
            return "redirect:/news/view/" + news_id;
        }
        Date current_date = new Date();
        comment.setCreation_date(current_date);
        commentService.addComment(comment);
        return "redirect:/news/view/" + news_id;
    }

    @RequestMapping(value = "/delete/{news_id}/{comment_id}", method = RequestMethod.POST)
    public String deleteComment(@PathVariable(NEWS_ID_KEY) Long news_id,
                                @PathVariable(COMMENT_ID_KEY) Long comment_id) throws ServiceException {
        commentService.deleteComment(comment_id);
        return "redirect:/news/view/" + news_id;
    }

}
