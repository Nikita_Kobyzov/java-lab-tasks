<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<div class="row">
    <div class="col-sm-1">
    </div>
    <div class="col-lg-10">
        <c:url var="tag_add" value="/tag/add/" context="${pageContext.request.contextPath}"/>
        <sf:form modelAttribute="tag" action="${tag_add}" cssClass="form-inline" method="post">
            <sf:input path="tag_name" cssClass="form-control"/>
            <input type="submit" class="btn btn-info" value="Add"><br>
            <sf:errors path="tag_name" cssStyle="color: #f39c12"/>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        </sf:form>
        <c:forEach var="tag" items="${tagList}">
            <form class="form-inline" method="post" id="updateTag${tag.tag_id}">
                <input name="tag_id" type="hidden" value="${tag.tag_id}">
        <span>
            <input id="name${tag.tag_id}" class="form-control" type="text" value="${tag.tag_name}"
                   readonly="readonly" name="tag_name"/>
            <a id="edit${tag.tag_id}" href="#" onclick="view(${tag.tag_id})">edit</a>
            <span id="buttons${tag.tag_id}" hidden="">
                <a href="#" onclick="updateTag(${tag.tag_id})">update</a>
                <a href="#" onclick="deleteTag(${tag.tag_id})">delete</a>
                <a href="#" onclick="hide(${tag.tag_id})">cancel</a>
            </span>
        </span>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
            <c:url var="tag_delete" value="/tag/delete/" context="${pageContext.request.contextPath}"/>
            <form method="post" action="${tag_delete}" id="deleteTag${tag.tag_id}">
                <input name="tag_id" type="hidden" value="${tag.tag_id}">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>
        </c:forEach>

    </div>
    <br><br><br>
</div>