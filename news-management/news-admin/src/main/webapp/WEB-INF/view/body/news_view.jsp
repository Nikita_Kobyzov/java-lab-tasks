<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<h2>${news_to.news.title} (by ${news_to.author.author_name})</h2>

<p>${news_to.news.full_text}</p>
<hr>
<nav>
    <ul class="pager">
        <c:if test="${pager.previous_id != null}">
            <c:url var="news_prev" value="/news/view/${pager.previous_id}" context="${pageContext.request.contextPath}">
                <c:forEach items="${criteria.authorList}" var="id">
                    <c:param name="authorList" value="${id}"/>
                </c:forEach>
                <c:forEach items="${criteria.tagList}" var="id">
                    <c:param name="tagList" value="${id}"/>
                </c:forEach>
            </c:url>
            <li><a href="${news_prev}">Previous</a></li>
        </c:if>
        <c:if test="${pager.next_id != null}">
            <c:url var="news_next" value="/news/view/${pager.next_id}" context="${pageContext.request.contextPath}">
                <c:forEach items="${criteria.authorList}" var="id">
                    <c:param name="authorList" value="${id}"/>
                </c:forEach>
                <c:forEach items="${criteria.tagList}" var="id">
                    <c:param name="tagList" value="${id}"/>
                </c:forEach>
            </c:url>
            <li><a href="${news_next}">Next</a></li>
        </c:if>

    </ul>
</nav>
<hr>
<c:url var="com_add" value="/comment/add/${news_to.news.news_id}" context="${pageContext.request.contextPath}"/>
<sf:form action="${com_add}" modelAttribute="comment">
    <div class="form-group">
        <sf:textarea path="comment_text" cssClass="form-control" rows="3"></sf:textarea>
        <sf:errors path="comment_text" cssStyle="color: #f39c12"/>
    </div>
    <button type="submit" class="btn btn-default">
        <span class="glyphicon glyphicon-comment"></span> Add comment
    </button>
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</sf:form>
<c:forEach items="${news_to.commentList}" var="comment">
            <span>
                <fmt:formatDate value="${comment.creation_date}" pattern="MM/dd/YYYY"/>
            </span>

    <div class="row alert alert-info">
        <div class="col-lg-11">
            <p class="text-left">${comment.comment_text}</p>
        </div>
        <div class="col-sm-1">
            <button type="submit" form="form-${comment.comment_id}" class="btn btn-link">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
        </div>
    </div>
    <c:url var="com_delete" value="/comment/delete/${news_to.news.news_id}/${comment.comment_id}" context="${pageContext.request.contextPath}"/>
    <form method="post" id="form-${comment.comment_id}"
          action="${com_delete}">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
</c:forEach>
