<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="<c:url value="/resources/css/bootstrap.min.css" context="${pageContext.request.contextPath}"/>">
<script src="<c:url value="/resources/js/jquery.min.js" context="${pageContext.request.contextPath}"/>"></script>
<script src="<c:url value="/resources/js/bootstrap.min.js" context="${pageContext.request.contextPath}"/>"></script>
<script src="<c:url value="/resources/js/script.js" context="${pageContext.request.contextPath}"/>"></script>