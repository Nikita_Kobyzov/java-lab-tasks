<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<security:authorize access="hasRole('ROLE_ADMIN')">
    <ul class="nav nav-pills nav-stacked">
        <li><a href="<c:url value="/news/" context="${pageContext.request.contextPath}"/>">Main page</a></li>
        <li><a href="<c:url value="/news/add" context="${pageContext.request.contextPath}"/>">Add news</a></li>
        <li><a href="<c:url value="/author/edit" context="${pageContext.request.contextPath}"/>">Add/edit authors</a></li>
        <li><a href="<c:url value="/tag/edit" context="${pageContext.request.contextPath}"/>">Add/edit tags</a></li>
    </ul>
</security:authorize>

