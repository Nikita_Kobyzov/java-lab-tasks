package by.epam.newsmanagement.command.request_parser.impl;

import by.epam.newsmanagement.command.request_parser.RequestParser;
import by.epam.newsmanagement.domain.Comment;
import by.epam.newsmanagement.exception.ClientCommandException;
import by.epam.newsmanagement.exception.ParserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
public class CommentParser implements RequestParser<Comment>{

    private static final String COMMENT_TEXT_KEY = "comment_text";
    @Autowired
    NewsIdParser newsIdParser;

    @Override
    public Comment parse(HttpServletRequest request) throws ParserException {
        String comment_text = request.getParameter(COMMENT_TEXT_KEY);
        long news_id = newsIdParser.parse(request);
        if(comment_text == null){
            throw new ParserException("Parameter " + COMMENT_TEXT_KEY + "is not found");
        }
        Comment comment = new Comment();
        comment.setNews_id(news_id);
        comment.setComment_text(comment_text);
        return comment;
    }
}
