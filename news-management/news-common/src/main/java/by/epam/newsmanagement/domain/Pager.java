package by.epam.newsmanagement.domain;

/**
 * <p>
 *     Objects of this class are used to indicate the next and previous news id concerning current news
 * </p>
 */
public class Pager {
    private Long next_id;
    private Long previous_id;

    public Long getNext_id() {
        return next_id;
    }

    public void setNext_id(Long next_id) {
        this.next_id = next_id;
    }

    public Long getPrevious_id() {
        return previous_id;
    }

    public void setPrevious_id(Long previous_id) {
        this.previous_id = previous_id;
    }

    @Override
    public String toString() {
        return Pager.class.getName() + " [next_id=" + next_id + ", previous_id=" + previous_id + "]";
    }

}
