package by.epam.newsmanagement.service.impl;

import by.epam.newsmanagement.domain.SearchCriteria;
import by.epam.newsmanagement.domain.*;
import by.epam.newsmanagement.domain.dto.NewsDTO;
import by.epam.newsmanagement.service.*;
import by.epam.newsmanagement.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component("newsManagementService")
public class NewsManagementServiceImpl implements INewsManagementService {

    @Autowired
    private INewsService newsService;
    @Autowired
    private IAuthorService authorService;
    @Autowired
    private ITagService tagService;
    @Autowired
    private ICommentService commentService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteNews(long news_id) throws ServiceException {
        commentService.deleteCommentOnNewsId(news_id);
        newsService.deleteNews(news_id);
    }

    @Override
    public NewsRecord getNewsRecord(long news_id) throws ServiceException {
        NewsRecord newsRecord = new NewsRecord();
        News news = newsService.getNews(news_id);
        newsRecord.setNews(news);
        Author author = authorService.readAuthorByNewsId(news_id);
        newsRecord.setAuthorId(author.getAuthor_id());
        List<Tag> tagList = tagService.readByNewsId(news_id);
        newsRecord.setTagIdList(tagList.stream().map(Tag::getTag_id).collect(Collectors.toList()));
        return newsRecord;
    }

    @Override
    public NewsDTO getNewsTO(long news_id) throws ServiceException {
        NewsDTO newsDTO = new NewsDTO();
        News news = newsService.getNews(news_id);
        newsDTO.setNews(news);
        Author author = authorService.readAuthorByNewsId(news_id);
        newsDTO.setAuthor(author);
        List<Tag> tagList = tagService.readByNewsId(news_id);
        newsDTO.setTagList(tagList);
        List<Comment> commentList = commentService.readByNewsId(news_id);
        newsDTO.setCommentList(commentList);
        return newsDTO;
    }

    @Override
    public List<NewsDTO> getNewsBySearchCriteria(Long page, SearchCriteria searchCriteria) throws ServiceException {
        List<News> newsList = newsService.getNewsBySearchCriteria(page, searchCriteria);
        List<NewsDTO> newsDTOList = new ArrayList<NewsDTO>(newsList.size());
        for (News news : newsList) {
            NewsDTO newsDTO = new NewsDTO();
            newsDTO.setNews(news);
            Author author = authorService.readAuthorByNewsId(news.getNews_id());
            newsDTO.setAuthor(author);
            List<Tag> tagList = tagService.readByNewsId(news.getNews_id());
            newsDTO.setTagList(tagList);
            List<Comment> commentList = commentService.readByNewsId(news.getNews_id());
            newsDTO.setCommentList(commentList);
            newsDTOList.add(newsDTO);
        }
        return newsDTOList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editNews(long news_id, NewsRecord newsRecord) throws ServiceException {
        newsService.editNews(news_id, newsRecord.getNews());
        Author author = authorService.readAuthorByNewsId(news_id);
        if (!author.getAuthor_id().equals(newsRecord.getAuthorId())) {
            newsService.editNewsAuthorConnection(news_id, newsRecord.getAuthorId());
        }
        if(newsRecord.getTagIdList() == null){
            newsRecord.setTagIdList(Collections.EMPTY_LIST);
        }
        newsService.editNewsTagsConnection(news_id,  newsRecord.getTagIdList());
    }
}
