package by.epam.newsmanagement.service.impl;

import java.util.Collections;
import java.util.List;

import by.epam.newsmanagement.domain.NewsRecord;
import by.epam.newsmanagement.domain.Pager;
import by.epam.newsmanagement.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import by.epam.newsmanagement.dao.INewsDao;
import by.epam.newsmanagement.dao.exception.DaoException;
import by.epam.newsmanagement.domain.SearchCriteria;
import by.epam.newsmanagement.domain.News;
import by.epam.newsmanagement.service.exception.ServiceException;

/**
 * <p>
 * This class is designed to solve problems of business logic for a News-entity
 * </p>
 *
 * @author Mikita_Kobyzau
 */
@Service("newsService")
public class NewsServiceImpl implements INewsService {

    private static final int NEWS_PER_PAGE = 5;
    @Autowired
    private INewsDao newsDao;

    @Override
    public List<News> getNewsBySearchCriteria(Long page, SearchCriteria searchCriteria) throws ServiceException {
        try {
            List<News> newsList = newsDao.readByCriteria(page, NEWS_PER_PAGE, searchCriteria);
            return newsList;
        } catch (DaoException e) {
            throw new ServiceException("Exception in News Service with page = "
                    + page + ", searchCriteria = " + searchCriteria, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean addNews(NewsRecord newsRecord) throws ServiceException {
        try {
            long news_id = newsDao.create(newsRecord.getNews());
            newsDao.connectNewsWithAuthor(news_id, newsRecord.getAuthorId());
            if(newsRecord.getTagIdList() == null){
                newsRecord.setTagIdList(Collections.EMPTY_LIST);
            }
            for (Long tag_id : newsRecord.getTagIdList()) {
                newsDao.connectNewsWithTag(news_id, tag_id);
            }
        } catch (DaoException e) {
            throw new ServiceException("Exception in News Service with newsRecord = " + newsRecord, e);
        }
        return true;
    }

    @Override
    public void editNews(long news_id, News news) throws ServiceException {
        try {
            newsDao.update(news_id, news);
        } catch (DaoException e) {
            throw new ServiceException("Exception in News Service with news_id = "
                    + news_id + ", news = " + news, e);
        }
    }

    @Override
    public News getNews(long news_id) throws ServiceException {
        try {
            News news = newsDao.read(news_id);
            return news;
        } catch (DaoException e) {
            throw new ServiceException("Exception in News Service with news_id = " + news_id, e);
        }
    }

    @Override
    public Long countOfPages(SearchCriteria criteria) throws ServiceException {
        try {
            long countOfNews = newsDao.countOfNews(criteria);
            long countOfPages = (countOfNews % NEWS_PER_PAGE == 0)
                    ? countOfNews / NEWS_PER_PAGE : countOfNews / NEWS_PER_PAGE + 1;
            return countOfPages;
        } catch (DaoException e) {
            throw new ServiceException("Exception in News Service", e);
        }
    }

    @Override
    public Pager getPager(long news_id, SearchCriteria criteria) throws ServiceException {
        try{
            return newsDao.getPager(news_id, criteria);
        }catch (DaoException e){
            throw new ServiceException("Exception in News Service with news_id = " + news_id
                    + ", criteria = " + criteria, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editNewsAuthorConnection(Long news_id, Long new_author_id) throws ServiceException {
        try {
            newsDao.deleteNewsAuthor(news_id);
            newsDao.connectNewsWithAuthor(news_id, new_author_id);
        } catch (DaoException e) {
            throw new ServiceException("Exception in News Service news_id = "
                    + news_id + ", new_author_id = " + new_author_id, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editNewsTagsConnection(Long news_id, List<Long> new_tag_id_list)
            throws ServiceException {

        try {
            newsDao.deleteNewsTag(news_id);
            for (Long tag_id : new_tag_id_list) {
                newsDao.connectNewsWithTag(news_id, tag_id);
            }
        } catch (DaoException e) {
            throw new ServiceException("Exception in News Service with news_id = "
                    + news_id + ", new_tag_id_list = " + new_tag_id_list, e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteNews(long news_id) throws ServiceException {
        try {
            newsDao.deleteNewsTag(news_id);
            newsDao.deleteNewsAuthor(news_id);
            newsDao.delete(news_id);
        } catch (DaoException e) {
            throw new ServiceException("Exception in News Service with news_id = " + news_id, e);
        }
    }

}
